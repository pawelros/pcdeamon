﻿using System;
using System.ServiceProcess;
using PcDeamon.Server;

namespace PcDeamon.Service
{
    public partial class Service1 : ServiceBase
    {
        private DeamonServer deamonServer;
        public Service1()
        {
            InitializeComponent();
            //if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            //{
            //    System.Diagnostics.EventLog.CreateEventSource(
            //        "MySource", "MyNewLog");
            //}
            //eventLog1.Source = "MySource";
            //eventLog1.Log = "MyNewLog";
            //FileLogger.EventLog = eventLog1;
        }

        protected override void OnStart(string[] args)
        {
        //    eventLog1.WriteEntry("In OnStart");
            try
            {
                this.deamonServer = new DeamonServer(this.eventLog1);
                this.deamonServer.Start();
            }
            catch (Exception ex)
            {
       //         eventLog1.WriteEntry(ex.ToString());
            }

        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("In onStop.");
            this.deamonServer.CancellationTokenSource.Cancel();
            this.deamonServer = null;
        }
    }
}
