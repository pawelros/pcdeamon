﻿using System;
using System.ComponentModel;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace Blokada_grania
{
    public class ServiceController : INotifyPropertyChanged
    {
        private StreamSocket clientSocket;
        public bool IsConnected { get; private set; }
        private string status;

        public string Status
        {
            get { return this.status; }
            private set
            {
                this.status = value;
                NotifyPropertyChanged("Status");
            }
        }

        public ServiceController()
        {
            this.IsConnected = false;
            this.Status = "Łączenie z komputerem...";
        }

        public async void Connect()
        {
            if (IsConnected)
            {
                return;
            }

            try
            {
                this.clientSocket = new StreamSocket();
                HostName hostname = new HostName(Consts.ServerIP);
                await this.clientSocket.ConnectAsync(hostname, Consts.ServerPort);
                IsConnected = true;
                Status = "Połączono z komputerem." + Environment.NewLine;
            }
            catch (Exception exception)
            {
                Status = "Nie udało się połączyć z komputerem: ";

                if (SocketError.GetStatus(exception.HResult) == SocketErrorStatus.ConnectionTimedOut)
                {
                    Status += "prawdopodobnie komputer jest wyłączony.";
                }
                else
                {
                    Status += exception.Message;   
                }
                 
                clientSocket.Dispose();
            }
        }

        public async void Send(string message)
        {
            if (!IsConnected)
            {
                Status = "Nie połączono jeszcze z komputerem!";
                return;
            }

            try
            {
               
                Status = "Wysyłanie żądania...";
                DataWriter writer = new DataWriter(clientSocket.OutputStream);
                writer.WriteString(message);
            
                await writer.StoreAsync();
                
                Status = "Żądanie wysłane, oczekiwanie na odpowiedź..." + Environment.NewLine;
                
                writer.DetachStream();
                writer.Dispose();

            }
            catch (Exception exception)
            {
                Status = "Błąd wysyłania żądania: " + exception.Message;

                clientSocket.Dispose();
                clientSocket = null;
                IsConnected = false;
            }

            try
            {
                if (clientSocket != null)
                {
                    DataReader reader = new DataReader(clientSocket.InputStream);
                    reader.InputStreamOptions =InputStreamOptions.Partial;

                    await reader.LoadAsync(512);
             
                    string data = reader.ReadString(reader.UnconsumedBufferLength);
                    Status = data;
                }
                else
                {
                    Connect();
                }
            }
            catch (Exception exception)
            {
                Status = "Wysyłanie żądania nie powiodło się: " + exception.Message;

                if (clientSocket != null)
                {
                    clientSocket.Dispose();
                }
                clientSocket = null;
                IsConnected = false;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
