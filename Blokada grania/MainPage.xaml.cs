﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace Blokada_grania
{
    public partial class MainPage
    {
        private readonly ServiceController serviceController;

        public MainPage()
        {
            InitializeComponent();
            this.serviceController = new ServiceController();
            this.DataContext = this.serviceController;
            Connect();
        }

        private void Connect()
        {
            this.serviceController.Connect();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button s = sender as Button;

            if (s != null)
                switch (s.Name)
                {
                    case "TurnOffPc":
                        this.serviceController.Send("TURN_OFF_PC");
                        break;
                    case "Logoff":
                        this.serviceController.Send("LOGOFF");
                        break;
                    default:
                        throw new InvalidOperationException("action is not supported");
                }
        }
    }
}