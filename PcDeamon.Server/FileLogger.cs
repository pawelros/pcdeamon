﻿using System;
using System.Diagnostics;
using System.IO;

namespace PcDeamon.Server
{
    public static class FileLogger
    {
        public const string LogPath = @"d:\";
        private static readonly object lockObject = new object();
        public static EventLog EventLog { get; set; }

        public static void WriteDebugMessage(string message)
        {
            lock (lockObject)
            {
                if (!Directory.Exists(LogPath))
                {
                    Directory.CreateDirectory(LogPath);
                }

                string filename = string.Concat(LogPath, "debug", DateTime.Now.ToString("yyyyMMdd"), ".txt");

                StreamWriter log = !File.Exists(filename) ? new StreamWriter(filename) : File.AppendText(filename);

                message = string.Concat(DateTime.Now.ToString("HH:mm:ss"), " ", message);
           //     Console.WriteLine(message);
                // Write to the file:
                log.WriteLine(message);
                if (EventLog != null)
                {
                    EventLog.WriteEntry(message);
                }

                // Close the stream:
                log.Close();
            }
        }

        public static void WriteLogMessage(string message)
        {
            lock (lockObject)
            {
                if (!Directory.Exists(LogPath))
                {
                    Directory.CreateDirectory(LogPath);
                }

                string filename = string.Concat(LogPath, "log", DateTime.Now.ToString("yyyyMMdd"), ".txt");

                StreamWriter log = !File.Exists(filename) ? new StreamWriter(filename) : File.AppendText(filename);

                // Write to the file:
                log.Write(DateTime.Now.ToString("HH:mm:ss"));
                log.Write(" ");
                log.WriteLine(message);

                // Close the stream:
                log.Close();
            }
        }

        public static void WriteException(Exception ex)
        {
            lock (lockObject)
            {
                if (!Directory.Exists(LogPath))
                {
                    Directory.CreateDirectory(LogPath);
                }

                string filename = string.Concat(LogPath, "exception", DateTime.Now.ToString("yyyyMMdd"), ".txt");

                StreamWriter log = !File.Exists(filename) ? new StreamWriter(filename) : File.AppendText(filename);

                // Write to the file:
                log.Write(DateTime.Now.ToString("HH:mm:ss"));
                log.Write(" ");
                log.WriteLine(ex);

                // Close the stream:
                log.Close();
            }
        }
    }
}
