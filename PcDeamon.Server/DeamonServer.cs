﻿using System.Diagnostics;
using PcDeamon.Server.Properties;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace PcDeamon.Server
{
    public class DeamonServer
    {
        private readonly int port;
        private readonly IPAddress localAddr;
        public CancellationTokenSource CancellationTokenSource { get; set; }
        private TaskFactory taskFactory;
        private EventLog eventLog;


        public DeamonServer(EventLog eventLog)
        {
            this.port = Settings.Default.Port;
            this.localAddr = IPAddress.Parse(Settings.Default.LocalAddr);
            this.CancellationTokenSource = new CancellationTokenSource();
            this.eventLog = eventLog;
            FileLogger.WriteLogMessage("DeamonServer initialized");
        }

        public void Start()
        {
            taskFactory = new TaskFactory();
            this.taskFactory.StartNew(() =>
            {
                FileLogger.WriteLogMessage(string.Format("Attempting to start the server on IP: {0} port: {1}",
                    this.localAddr, this.port));
                TcpListener tcpListener = CreateTcpListener();

                while (!CancellationTokenSource.IsCancellationRequested)
                {
                    try
                    {
                        FileLogger.WriteLogMessage("Waiting for incoming connections...");
                        Socket socket = tcpListener.AcceptSocket();
                        FileLogger.WriteLogMessage("Accepted new connection");
                        TakeControllOverClient(socket);
                    }
                    catch (Exception ex)
                    {
                        FileLogger.WriteException(ex);
                    }
                }
            });
        }

        private void TakeControllOverClient(Socket socket)
        {
            ClientController clientController = new ClientController(socket, CancellationTokenSource);
            taskFactory.StartNew(clientController.AcceptRequests, CancellationTokenSource.Token);
        }

        private TcpListener CreateTcpListener()
        {
            TcpListener tcpListener = new TcpListener(localAddr, port);
            FileLogger.WriteLogMessage("Run TCP Listener...");
            tcpListener.Start();
            FileLogger.WriteLogMessage("TCP Listener started");
            return tcpListener;
        }
    }
}
