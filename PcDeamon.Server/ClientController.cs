﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace PcDeamon.Server
{
    internal class ClientController
    {
        private readonly Socket socket;
        private readonly CancellationTokenSource cancellationTokenSource;
        public ClientController(Socket socket, CancellationTokenSource cancellationTokenSource)
        {
            this.socket = socket;
            this.cancellationTokenSource = cancellationTokenSource;
        }

        public void AcceptRequests()
        {
            FileLogger.WriteDebugMessage(string.Format("{0} connection from host {1} accepted", this, socket.RemoteEndPoint));
            Byte[] byteBuffer = new Byte[1024];
            while (socket.Connected && !cancellationTokenSource.IsCancellationRequested)
            {
                int size = socket.Receive(byteBuffer);
                string data = Encoding.ASCII.GetString(byteBuffer, 0, size);
                if (!string.IsNullOrWhiteSpace(data))
                {
                    FileLogger.WriteDebugMessage(string.Format("Thread Id {0} received data: {1}",
                        Thread.CurrentThread.ManagedThreadId, data));
                    string response = ExecuteRequest(data);
                    socket.Send(Encoding.ASCII.GetBytes(response));
                }
            }
            socket.Close();
            FileLogger.WriteDebugMessage(string.Format("Thread Id {0} connection closed", Thread.CurrentThread.ManagedThreadId));
        }

        private string ExecuteRequest(string request)
        {
            
            int time = 5;
            try
            {
                switch (request)
                {
                    case Consts.TurnOffPc:
                        TurnOffPc(time);
                        break;
                    case Consts.Logoff:
                        LogOff(time);
                        break;
                    default: throw new NotSupportedException("the requested action is not supported");
                }
            }
            catch (Exception ex)
            {
                FileLogger.WriteException(ex);
                return "ERR";
            }
            return "OK";
        }


        private void TurnOffPc(int time)
        {
            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processInfo.FileName = "cmd.exe";
            processInfo.WorkingDirectory = Path.GetDirectoryName("c:\\");
            processInfo.Arguments = string.Format("/c shutdown -s -f -t {0}", time);
            Process.Start(processInfo);
        }

        private void LogOff(int time)
        {
            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.WindowStyle = ProcessWindowStyle.Hidden;
            processInfo.FileName = "cmd.exe";
            processInfo.WorkingDirectory = Path.GetDirectoryName("c:\\");
            processInfo.Arguments = string.Format("/c shutdown -l -f");
            Process.Start(processInfo);
        }

    }
}
