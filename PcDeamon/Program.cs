﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PcDeamon.Server;

namespace PcDeamon
{
    class Program
    {
        static void Main(string[] args)
        {
            //   DeamonServer server = new DeamonServer(null);

            //   Console.WriteLine("Waiting for incoming connections...");
            //   server.Start();
            EmulateClients(new CancellationTokenSource());
            //RouterClient client = new RouterClient();
            //string result =  client.LogIn().Result;
        }


        public static void EmulateClients(CancellationTokenSource cancellationTokenSource)
        {
            TaskFactory taskFactory = new TaskFactory();
            for (int i = 1; i <= 5; i++)
            {
                taskFactory.StartNew(ConnectToServer, cancellationTokenSource.Token);
            }
            taskFactory.StartNew(() => { Thread.Sleep(10000); cancellationTokenSource.Cancel(); });
        }

        public static void ConnectToServer()
        {
            Thread.Sleep(5000);
            Socket clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                clientSocket.Connect(new IPEndPoint(IPAddress.Parse("192.168.1.3"), 3333));

                for (int i = 0; i < 10; i++)
                {
                    if (clientSocket.Connected)
                    {
                        clientSocket.Send(Encoding.ASCII.GetBytes(i + " siemanko"));
                        Thread.Sleep(3000);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (clientSocket.Connected)
                {
                    clientSocket.Disconnect(true);
                }
            }

        }
    }
}
